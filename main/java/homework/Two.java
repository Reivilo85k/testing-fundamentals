package homework;

public class Two {

    /**
     * Let's implement demerit point calculator in traffic
     * <p>
     * - if a driver speed is equal or smaller than speed limit,
     * demerit point will be 0
     * <p>
     * - if a driver speed is greater than speed limit,
     * demerit points will be calculated by this rule :
     * * demerit point will increase 1 point per 5 km
     * <p>
     * - speed limit is 65
     */

    private int speed;
    private int demerit;
    private int distance;
    private boolean exceedsLimit;

    public Two() {
    }

    public Two(int speed, int demerit, int distance, boolean exceedsLimit) {
        this.speed = speed;
        this.demerit = demerit;
        this.distance = distance;
        this.exceedsLimit = exceedsLimit;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public boolean isExceedsLimit() {
        return exceedsLimit;
    }

    public void setExceedsLimit(boolean exceedsLimit) {
        this.exceedsLimit = exceedsLimit;
    }

    public int getDemerit() {
        return demerit;
    }

    public void setDemerit(int demerit) {
        this.demerit = demerit;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public boolean checkSpeed(int speed, boolean exceedsLimit) {
        int limit = 65;
        if (speed > limit) {
            exceedsLimit = true;
        }return exceedsLimit;
    }

    public int calculateDemerit(int demerit, int distance, boolean exceedsLimit){
        if (exceedsLimit){
            demerit = (distance / 5);
        }return demerit;
    }

    public static void main(String[] args) {
        Two driver = new Two (80, 0, 1500, false);

        driver.setExceedsLimit(driver.checkSpeed(driver.getSpeed(), driver.isExceedsLimit()));
        driver.setDemerit(driver.calculateDemerit(driver.getDemerit(), driver.getDistance(), driver.isExceedsLimit()));

        System.out.println(driver.getDemerit());
    }
}
