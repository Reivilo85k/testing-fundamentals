package homework;

import java.util.Scanner;

public class One {

    /**
     *
     * Try to implement a method that calculates
     * factorial number for given input by applying
     * TDD
     *
     */

    public static long calculateFactorial(long input){
        long result = input;
        for (long i = input; i > 1; i--){
            result = result*(i-1);
        }return result;
    }

    public static String displayResult(long input,long result){
        return ("Factorial of " + input +" is: "+" 2432902008176640000");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Chose a number between 0 and 20");
        int input = scanner.nextInt();
        long result;

        if (input <= 20) {
           result = calculateFactorial(input);
           System.out.println(displayResult(input,result));
        }else System.out.println("Number too large");
    }
}

