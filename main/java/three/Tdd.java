package three;

public class Tdd {

/**
 *
 * Red
 * Green
 * Refactor
 *
 * Problem 1 : FizzBuzz
 *
 * Write a program that prints the numbers from 1 to 100.
 * But for multiples of three print “Fizz” instead
 * of the number and for the multiples of five print “Buzz”.
 * For numbers which are multiples of both three
 * and five print “FizzBuzz”.
 *
 * 	- Print numbers 1 to 100
 * 	- If divisible by 3 replace with "Fizz"
 * 	- If divisible by 5 replace with  "Buzz"
 * 	- If ….. 15 replace with "FizzBuzz"
 *
 */
}
