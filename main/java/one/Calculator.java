package one;

public class Calculator {

    public int add(int firstNumber, int secondNumber) {
        return firstNumber + secondNumber;
    }

    public int divide(int firstNumber, int secondNumber) {
        return firstNumber / secondNumber;
    }


    /**
     * Q1
     * Write a method that takes two input,
     * returns max number of the given input
     *
     * Q2
     * Write a method that gets a number,
     * returns odd numbers of starting from 0
     * to the upper range.
     */

}
