package one;

public class Database {

    public void connect(){
        System.out.println("Connecting to database...");
    }

    public void close(){
        System.out.println("Closing db connection...");
    }

    public Person updateName(Person person, String name){
        person.setName(name);
        System.out.println("Update is successful...");
        return person;
    }

    public Person insert(String name){
        Person person = new Person(name);
        System.out.println("New person has been created...");
        return person;
    }


}
