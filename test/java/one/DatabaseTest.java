package one;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class DatabaseTest {

    /**
     * Testing lifecycle
     * When to use them ?
     * - database, cache
     * - object recreation
     */

    private static Database database;

    @BeforeClass
    public static void init() {
        database = new Database();
        database.connect();
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("Run before each test execution");
    }

    @Test
    public void testInsertOnePersonReturnsTheSamePerson(){
        // Arrange
        String name = "Feride";

        //Act
        Person result = database.insert(name);

        //assert
        assertEquals(name, result.getName());
    }

    @Test
    public void testUpdateOnePersonReturnsUpdatedPerson(){
        // Arrange
        String name = "Feride";
        Person person = new Person(name);
        String updatedName = "Katrin";

        //Act
        Person result = database.updateName(person, updatedName);

        //assert
        assertEquals(updatedName, result.getName());
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Run after each test execution");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        database.close();
    }
}
