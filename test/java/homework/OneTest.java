package homework;

import org.junit.Test;

import static org.junit.Assert.*;

public class OneTest {

    @Test
    public void testInputInteger3Returns6() {
        //Arrange
        long input = 3;
        long expected = 6;

        //Act
        long result = One.calculateFactorial(input);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void testInputInteger5Returns120() {
        //Arrange
        long input = 5;
        long expected = 120;

        //Act
        long result = One.calculateFactorial(input);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void testInputInteger10Returns3628800() {
        //Arrange
        long input = 10;
        long expected = 3628800;

        //Act
        long result = One.calculateFactorial(input);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void testInputInteger18Returns6402373705728000() {
        //Arrange
        long input = 18;
        long expected = 6402373705728000l;

        //Act
        long result = One.calculateFactorial(input);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void testInputInteger20Returns2432902008176640000() {
        //Arrange
        long input = 20;
        long expected = 2432902008176640000l;

        //Act
        long result = One.calculateFactorial(input);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void testDisplayWithInteger10Displays3628800() {
        //Arrange
        long input = 10;
        long result = One.calculateFactorial(input);
        String expected = "Factorial of " + input + " is: " + " 2432902008176640000";

        //Act
        String display = One.displayResult(input, result);

        //Assert
        assertEquals(expected, display);

    }
    @Test
    public void testDisplayWithInteger20Displays2432902008176640000() {
        //Arrange
        long input = 20;
        long result = One.calculateFactorial(input);
        String expected = "Factorial of " + input + " is: " + " 2432902008176640000";

        //Act
        String display = One.displayResult(input, result);

        //Assert
        assertEquals(expected, display);

    }
}
