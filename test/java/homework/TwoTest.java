package homework;

import org.junit.Test;
import static org.junit.Assert.*;

public class TwoTest {

    @Test
    public void testCheckSpeedIfSpeedIsWithinLimits(){
        int speed = 50;
        Two driver = new Two(speed, 0,0,false);

        boolean result = driver.checkSpeed(speed, false);

        assertEquals(false, result);
    }
    @Test
    public void testCheckSpeedIfSpeedIsAtLimit(){
        int speed = 65;
        Two driver = new Two(speed, 0,0, false);

        boolean result = driver.checkSpeed(speed, false);

        assertEquals(false, result);
    }

    @Test
    public void testCheckSpeedIfSpeedIsOverLimit(){
        int speed = 70;
        Two driver = new Two(speed, 0,0,false);

        boolean result = driver.checkSpeed(speed, false);

        assertEquals(true, result);
    }
    @Test
    public void testCalculateDemeritIfOverLimit(){
        boolean exceedLimit = true;
        int expected = 4;
        Two driver = new Two();

        int result = driver.calculateDemerit(0, 20,true);

        assertEquals(expected, result);
    }

    @Test
    public void testCalculateDemeritIfUnderLimit(){
        boolean exceedLimit = true;
        int expected = 0;
        Two driver = new Two();

        int result = driver.calculateDemerit(0, 20,false);

        assertEquals(expected, result);
    }


}
